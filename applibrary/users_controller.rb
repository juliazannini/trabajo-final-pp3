class UsersController < ApplicationController
#before_action :require_user

#metodos de books#######################################################
  def books
    @books = Book.all
  end


  def show_book
    @book = Book.find(params[:id])
  end

  def search
    keyword = params[:keyword]
    @books = Book.search(keyword)
    #render plain: "#{@books}"
  end

  def search_loans
    keyword = params[:keyword]
    @loans = Loans.search(keyword)
    #render plain: "#{@books}"
  end

#metodos del loan###########################################################

  def loans
    @loans = Loan.all
  end


end
