Rails.application.routes.draw do
  resources:books
 #Session routes

 get 'visitors/index'
 root 'visitors#index'

 get 'visitors/search', to: 'visitors#search'

##################################################################
 get 'login', to: 'session#new'
 post 'login', to: 'session#create'
 delete 'logout', to: 'session#destroy'
 get 'logout', to: 'session#destroy'


#Controller user - book router##########################################
 get 'users/index'
 get'/users/books', to: 'users#books'
 get '/users/books/:id', to: 'users#show_book', as: 'show_users_books'
 get'/users/loans', to: 'users#loans'
 





#LIBRARIANS#################################################################
 #librarians Users routes############################################################
 get 'librarians/index'
 get'/librarians/users', to: 'librarians#users'
 match '/librarians/users/signup', to:'librarians#new_user', via: 'get'
 post '/librarians/users', to: 'librarians#create_user'
 get '/librarians/users/:id', to: 'librarians#show_user', as: 'show_librarians_users'
 get '/librarians/:id/edit_user', to: 'librarians#edit_user',as: 'edit_librarians_users'
 put '/librarians/users/:id', to: 'librarians#update_user'
 delete'/librarians/users/:id', to: 'librarians#delete_user', as: 'delete_librarians_users'


 #librarian loan route#############################################################
  get'/librarians/loans', to: 'librarians#loans'
  get'/librarians/loans/new_loan', to:'librarians#new_loan'
  post '/librarians/loans', to: 'librarians#create_loan'
  get '/librarians/:id/edit_loan', to: 'librarians#edit_loan',as: 'edit_librarians_loans'
  put '/librarians/loans/:id', to: 'librarians#update_loan'
  delete'/librarians/loans/:id', to: 'librarians#delete_loan', as: 'delete_librarians_loans'


#librarian routes books###########################################################
  get'/librarians/books', to: 'librarians#books'
  get '/librarians/books/new_book', to: 'librarians#new_book'
  post '/librarians/books', to: 'librarians#create_book'
  get '/librarians/books/:id', to: 'librarians#show_book', as: 'show_librarians_books'
  get '/librarians/:id/edit_book', to: 'librarians#edit_book',as: 'edit_librarians_books'
  put '/librarians/books/:id', to: 'librarians#update_book'
  delete'/librarians/books/:id', to: 'librarians#delete_book', as: 'delete_librarians_books'

end
