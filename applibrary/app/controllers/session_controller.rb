class SessionController < ApplicationController
  def new
   #formulario de la sesion
  end

  def create
    #puts params
    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      session[:user_id] = user.id
      if user.librarian?
        redirect_to librarians_index_path
      else
        redirect_to users_index_path
      end
    else
      redirect_to visitors_index_path
    end
  end


  def destroy
    session[:user_id] = nil
    redirect_to visitors_index_path
  end


end
