class UsersController < ApplicationController
before_action :require_user

 def index
   @current_user = User.find(session[:user_id])
 end


#metodos de books#######################################################
def books
  @books = Book.all

  if params[:keyword]
    @books = Book.search(params[:keyword])
  else

    @books = Book.all.order('created_at DESC')

  end
end


  def show_book
    @book = Book.find(params[:id])
  end



  #metodos del loan###########################################################

    def loans
        @current_user = User.find(session[:user_id])
        @loans = Loan.all
    end


end
