class PreceptorsController < ApplicationController
#before_action :require_preceptor


def loans
  @loans = Loan.all
end


def new_loan
  @loan = Loan.new
end


def create_loan
  @book = Book.find_by_id (params[:loan][:book_id])

  if @book.available?
    puts loan_params

    @loan = Loan.new(loan_params)
    @loan.save
    render plain:"Se ah creado el préstamo"
  else
    render plain:"Libro no disponible"
  end
end

def delete_loan
  @loan = Loan.find(params[:id])
  @loan.destroy
  redirect_to preceptors_loans_path
end

def edit_loan
  @loan = Loan.find(params[:id])
end

def update_loan
  @loan = Loan.find(params[:id])
  if @loan.update(loan_params)
    redirect_to preceptors_loans_path
  else
    render :edit_loan
  end
end

##METODOS BOOKS####################books#########################
def books
  @books = Book.all
end

def show_book
  @book = Book.find(params[:id])
end





####PARAMS PERMIT##############################################################

private


    def loan_params
      params.permit(:loan)
      #params.require(:loan).permit(:@user,:@book , :start_Date , :return_Date , :state )
    end

end
