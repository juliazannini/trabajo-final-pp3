class ApplicationController < ActionController::Base
  helper_method :current_user

  def current_user
    @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
  end

  def require_user
    redirect_to login_path unless current_user
  end

  def require_librarian
    unless current_user.librarian?
      flash[:notice] = "No tienes permisos de operador"
      redirect_to('/login'+"?redirect='#{request.original_fullpath}'")
    end
  end


end
