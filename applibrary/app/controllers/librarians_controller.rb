class LibrariansController < ApplicationController

  before_action :require_librarian


  #METODOS DEL LOAN #############################################################################··············································

  def loans
    @loans = Loan.all
    @loans = @loans.map { |loan| loan.expired? }
    if params[:keyword]
      @loans = Loan.search(params[:keyword])
    else
      @loans = Loan.all.order('created_at DESC')
    end
  end


  def new_loan
    @loan = Loan.new

  end


  def create_loan

    @book = Book.find_by_id(params[:loan][:book_id])
    if @book.available?
      #puts loan_params

      @loan = Loan.new(loan_params)
      @loan.save
      flash[:success]="Préstamo creado con éxito"
        redirect_to librarians_loans_path
    else
      flash[:success]="Libro no disponible"
        render :new_loan
    end
  end


  def delete_loan
    @loan = Loan.find(params[:id])
    @loan.destroy
    redirect_to librarians_loans_path
  end

  def edit_loan
    @loan = Loan.find(params[:id])
  end

  def update_loan
    @loan = Loan.find(params[:id])
    if @loan.update(loan_params)
      redirect_to librarians_loans_path
    else
      render :edit_loan
    end
  end



# acciones del book######################################################
  def books
    @books = Book.all
    #@books = @books.map { |book| book.anavailable? }

    if params[:keyword]
      @books = Book.search(params[:keyword])
    else

      @books = Book.all.order('created_at DESC')

    end
  end

  def new_book
    @book = Book.new
  end


  def create_book
    @book = Book.new(book_params)
     if @book.save
       flash[:success]="Libro creado con éxito"
       redirect_to librarians_books_path

     else
       render :new_book

     end
  end


  def show_book
    @book = Book.find(params[:id])
  end


  def delete_book
    @book = Book.find(params[:id])
    @book.destroy
    redirect_to librarians_books_path
  end

  def edit_book
    @book = Book.find(params[:id])
  end

  def update_book
    @book = Book.find(params[:id])
    if @book.update(book_params)
      redirect_to librarians_books_path
    else
      render :edit_book
    end
  end


#acciones del usurario#######################################################
  def users
    @users = User.all

    if params[:keyword]
      @users = User.search(params[:keyword])
    else
      @users = User.all.order('created_at DESC')
    end
  end



  def new_user
    @user = User.new
  end


  def create_user
      @user = User.new(user_params)
      @user.role = "user"
      if @user.save
        flash[:success]="Usuario registrado con éxito"
        redirect_to librarians_users_path
      else
      render :new_user
      end
  end


  def show_user
    @user = User.find(params[:id])
  end


  def delete_user
    @user = User.find(params[:id])
    @user.destroy
    redirect_to librarians_users_path
  end


  def edit_user
    @user = User.find(params[:id])
  end


  def update_user
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to librarians_users_path
    else
      render :edit_user
    end
  end



####PARAMS PERMIT##############################################################

private
    def user_params
      params.require(:user).permit( :id, :name , :lastname , :dni , :email , :phone , :password, :password_confirmation)
    end


    def book_params
      params.require(:book).permit(:title ,:author ,:editorial ,:year , :state ,:isbn)
    end


    def loan_params

      params.require(:loan).permit(:user_id,:book_id , :start_Date , :return_Date , :state )
    end

end
