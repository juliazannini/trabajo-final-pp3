class Book < ApplicationRecord
    has_many :loans
    has_many :users, through: :loans
    validates :state, inclusion: { in: %w(available anavailable ) }
    validates :title, :editorial , :author, :isbn,:year, presence: true
    validates :isbn, numericality: true
    validates :title,:author  ,length: { minimum: 3 }



    def available?
    loans.where(state:"actived"  ).or(loans.where( state: "expired")).count == 0
    end



    #def anavailable?
  #  if loans.where(state:"actived"  ).or(loans.where( state: "expired")).count==0
      #  self.state="anavailable"
      #  self.save
      #end
      #self.state=="anavailable"
    #end





    def self.search(palabra)
        Book.where("title LIKE ?  OR  author LIKE ?" ,palabra ,palabra)
    end






end
