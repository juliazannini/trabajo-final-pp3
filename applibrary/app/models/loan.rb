class Loan < ApplicationRecord
  belongs_to :user
  belongs_to :book
  validates :state, inclusion: { in: %w( actived finished expired ) }
  validates :user_id, :book_id ,:state, :start_Date,:return_Date, presence: true


def self.search(keyword)
  Loan.joins(:user,:book).where("(users.name LIKE ?) OR (books.title LIKE ?) OR (loans.state Like?)", keyword,keyword, keyword)
end

def expired?
  if self.return_Date < DateTime.now
    self.state= "expired"
    self.save
  end
  self.state=="expired"

end






end
