class User < ApplicationRecord
  has_secure_password
  has_many :loans
  has_many :books, through: :loans

  validates :email, uniqueness: true
  validates :name, :lastname , :phone , :dni , presence: true
  validates :dni, :phone , numericality: true
  validates :name,:lastname  ,length: { minimum: 3 }
  validates :dni, length: { is: 8 }
  validates :password, length: { in: 6..10 }

  validates :role, inclusion: { in: %w(librarian user ) }

  def librarian?
    self.role == 'librarian'
  end


  def self.search(palabra)
      User .where("dni LIKE ? " ,palabra)
  end



#filtar prestamo personal en user
#ver Libros por nombre, editorial, Autor y que se vea el estado

#filtrar para preceptor usuario en prestamo para verificar que no tenga prestamo
#filtar libros por nombre, editorial autosr, etc

end
