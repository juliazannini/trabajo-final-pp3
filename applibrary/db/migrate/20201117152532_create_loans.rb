class CreateLoans < ActiveRecord::Migration[6.0]
  def change
    create_table :loans do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.belongs_to :book, null: false, foreign_key: true
      t.datetime :start_Date
      t.datetime :return_Date
      t.string :state

      t.timestamps
    end
  end
end
