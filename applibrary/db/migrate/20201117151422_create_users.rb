class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :lastname
      t.integer :dni
      t.string :email, index: true, unique: true, null: false
      t.integer :phone
      t.string :password_digest
      t.string :role, default: 'user'

      t.timestamps null: false
    end
  end
end
